import React, { Component } from 'react'

export default class Welcome extends Component {
  render() {
    return (
      <div className='welcome'>
        <div className="container">
          <h1 className='text-center'>A warm welcome!</h1>
          <p>Bootstrap utility classes are used to create this jumbotron since the old component 
            has been removed from the framework. Why create custom CSS when you can use utilities?</p>
            <button className='btn btn-primary'>Call to action</button>
        </div>
      </div>
    )
  }
}
