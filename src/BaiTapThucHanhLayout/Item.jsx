import React, { Component } from 'react'

export default class Item extends Component {
  render() {
    return (
      <div className='item'>
          <div className="icon d-flex mx-auto">
            <i class="fa-solid fa-cloud-arrow-down"></i>
          </div>
          <h6>Free to download</h6>
          <p>As always, Start Bootstrap has a powerful collectin of free templates.</p>
      </div>
    )
  }
}
