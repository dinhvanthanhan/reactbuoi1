import React, { Component } from 'react'

export default class Footer extends Component {
  render() {
    return (
      <div className='footer bg-dark'>
        <div className="container">
          <p className='text-white'>Copyright © Your Website 2022</p>
        </div>
      </div>
    )
  }
}
