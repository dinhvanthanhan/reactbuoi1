import React, { Component } from 'react'

export default class Header extends Component {
  render() {
    return (
      <div className='header bg-dark'>
        <div className="container">
          <div className="header__content d-flex justify-content-between">
            <div className="logo"><a href="#">Start Bootstrap</a></div>
            <ul className='d-flex'>
              <li><a href="#" className='first mr-3'>Home</a></li>
              <li><a href="#" className='second mr-3'>About</a></li>
              <li><a href="#" className='third mr-3'>Contact</a></li>
            </ul>
          </div>
        </div>
      </div>
    )
  }
}
