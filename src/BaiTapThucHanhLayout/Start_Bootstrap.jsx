import React, { Component } from 'react'
import Footer from './Footer'
import Header from './Header'
import Item from './Item'
import Welcome from './Welcome'

export default class Start_Bootstrap extends Component {
    render() {
        return (
            <>
                <Header />
                <Welcome />
                <div className="container">
                    <div className='row'>
                        <div className="col-4">
                            <Item />
                        </div>
                        <div className="col-4">
                            <Item />
                        </div>
                        <div className="col-4">
                            <Item />
                        </div>
                        <div className="col-4">
                            <Item />
                        </div>
                        <div className="col-4">
                            <Item />
                        </div>
                        <div className="col-4">
                            <Item />
                        </div>
                    </div>
                </div>
                <Footer />
            </>
        )
    }
}
